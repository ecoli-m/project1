<?php
$data = [
    'success'=> 1,//0表示上传失败;1表示上传成功
    'message'=>'上传失败',//"提示的信息",
    'url'=>'abd.png'
];

if(isset($_FILES))
{
    $file = $_FILES['editormd-image-file'];
    $ext  = getExt($file['name']);
    $dir_name = 'images/';
    $file_name  = $dir_name.'/'.time().substr(uniqid(),-4).$ext;
    move_uploaded_file($file['tmp_name'], $file_name);
    $data['success']=1;
    $data['message']='上传成功';
    $data['url']=$file_name;
    //var_dump($data);

}
echo json_encode($data);

/**
 * @desc 获取文件后缀名
 * @return string
 */
function getExt($filename)
{
   $pos = strrpos($filename, '.');
   $ext = substr($filename, $pos);
   return $ext;
}

//echo json_encode($data);

