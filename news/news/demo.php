<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Responsive Mail Inbox and Compose - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/blog.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
        $id=$_GET['id'];
        require '../getinfo.php';
    ?>
    <div class="container-fluid">
        <link rel='stylesheet prefetch'
            href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                    <a class="inbox-avatar" href="../myblog.php?author=<?php echo $username;?>&page=1">
                        <img width="64" hieght="60" src="../../<?php echo $savepath;?>">
                    </a>
                    <div class="user-name">
                        <h5><a href="../myblog.php?author=<?php echo $username;?>&page=1"><?php echo $username;?></a></h5>

                        <span><a href="../myblog.php?author=<?php echo $username;?>&page=1"><?php echo $email;?></a></span>
                    </div>
                    <div class="btn-group" style="float: right;top: 10px;">
                        <a class="btn mini btn-success" href="../../userinfo.php">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                </div>
                <ul class="inbox-nav inbox-divider">
                    <li class="active">
                        <a href="../showall.php?category=&page=1"><i class="fa fa-inbox"></i> 全部 </a>
                    </li>
                    <li>
                        <a href="../showall.php?category=ty&page=1"><i class="fa fa-bookmark-o"></i> 体育</a>
                    </li>
                    <li>
                        <a href="../showall.php?category=cj&page=1"><i class="fa fa-bookmark-o"></i> 财经</a>
                    </li>
                    <li>
                        <a href="../showall.php?category=gj&page=1"><i class="fa fa-bookmark-o"></i> 国际</a>
                    </li>
                    <li>
                        <a href="../showall.php?category=sh&page=1"><i class=" fa fa-trash-o"></i> 社会</a>
                    </li>
                </ul>
                <div class="blank"> </div>
                <div class="inbox-body text-center">
                    <div class="btn-group">
                        <a class="btn btn-lg btn-danger" href="../../logout.php">
                            退出
                        </a>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">

                    <ul class="nav navbar-nav">

                        <li><a style="color:inherit;" href="../../admin.php">新闻</a></li>
                        <li><a style="color:inherit;" href="../blog.php?page=1">博客</a></li>
                    </ul>
                    <form action="../getnews/search.php" class="pull-right position">
                        <div class="input-append">
                            <input type="text" class="sr-input" placeholder="Search Blog" id ="search" name="search">
                            <button class="btn sr-btn" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="inbox-body col-lg-10 col-lg-offset-1"  >
                    <div style="">
                    <?php
                    $file=$id.".html"; 
                    require_once('../getnews/conn.php');
                    $dbc = mysqli_connect(HOST,USER,PASS,DBN)
                              or die ("connected error"); 
                    $query = "SELECT * FROM `wynews`  WHERE id=$id";
                    $result = mysqli_query($dbc,$query)
                              or die ("quering error");
                    $data=mysqli_fetch_array($result);
                    ?>
                    <h1><?php echo $data['title'];?></h1>
                    <p><?php echo $data['post_info'];?></p>
                    <?require_once("$file"); ?>
                    </div>
                </div>
                <form>
                <div>
                    <p class="small">评论区</p>
                    <input type="text" class="hidden" name="article_id" value="<?php echo $id;?>">
                    <textarea class="form-control" rows="3" name="content"></textarea>
                    <div class="text-right"><button type="submit" class="btn btn-primary btn-xs"
                        style="margin-top: 8px; margin-bottom: 15px;" name="comment" formaction="../preserve_comment.php?type=news" 
                        formmethod="POST">提交评论</button></div>
                </div>
                </form>
                <div><p id="content" class="small">
                    <?php
                        require '../conn_comment.php';
                        $comment_res = mysqli_query($conn_comment,"SELECT * FROM news_comment WHERE article_id = '$id'  ORDER BY create_time DESC");
                        function myresult($result,$row,$type){
                            mysqli_data_seek($result, $row);
                            $row = mysqli_fetch_array($result);
                            return $row[$type];
                        }
                        function selectinfo($stuNum,$type){
                            require '../conn.php';
                            $sql="select * from infoset where stuNum=$stuNum";
                            $result=mysqli_query($conn,$sql)
                                or die("quering error");
                            $row=mysqli_fetch_array($result);
                            return $row[$type];
                        }
                        for($i = 0; $i < mysqli_num_rows($comment_res); $i++)
                        {
                            $create_time = myresult($comment_res, $i, 'create_time');
                            $comm_content = myresult($comment_res, $i, 'content');
                            $user_id = myresult($comment_res,$i,'user_id');
                            $username = selectinfo($user_id,'username');
                            $savepath = selectinfo($user_id,'save_path');
                            ?>
                            <div class="list-item">
                                <div class="user-face">
                                    <a href="../myblog.php?author=<?php echo $username;?>&page=1" target="_blank">
                                        <div class="bili-avatar">
                                            <img class="bili-avatar-img bili-avatar-face" src="../../<?php echo $savepath;?>" alt="" width="64" hieght="60">
                                        </div>
                                    </a>
                                </div>
                                <div class="con ">
                                    <div class="user">
                                        <a href="../myblog.php?author=<?php echo $username;?>&page=1" class="name"><?php echo $username;?></a>
                                    </div>
                                    <p class="text">
                                        <?php echo $comm_content;?>
                                    </p>
                                    <div class="info">
                                        <span class="time"><?php echo $create_time;?></span>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    ?>
                </p></div>
            </aside>
        </div>
    </div>
    <script type="text/javascript">

    </script>
</body>

</html>