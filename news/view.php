<?php
    $id=$_GET['id'];
    require('./conn.php');
    $result=mysqli_query($conn,"SELECT * From article Where id='{$id}'")
        or die("query failed");
    $row = mysqli_fetch_array($result,MYSQLI_BOTH);
    $content=$row['content'];
    $title=$row['title'];
    $judgetemp=$row['judgetemp'];

    require './conn_comment.php';
    $comment_res = mysqli_query($conn_comment,"SELECT * FROM user_comment WHERE article_id = '$id'  ORDER BY create_time DESC");
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>View</title>

    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="./md/css/editormd.css" />

    <script src="./js/jquery.min.js"></script>
    <script src="./md/editormd.js"></script>
    <script src="./md/lib/marked.min.js"></script>
    <script src="./md/lib/prettify.min.js"></script>
    <script src="./md/lib/raphael.min.js"></script>
    <script src="./md/lib/underscore.min.js"></script>
    <script src="./md/lib/sequence-diagram.min.js"></script>
    <script src="./md/lib/flowchart.min.js"></script>
    <script src="./md/lib/jquery.flowchart.min.js"></script>

</head>

<body>
    <?php
        require './getinfo.php';
    ?>
    <div class="container-fluid">
        <link rel='stylesheet prefetch'
            href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                    <a class="inbox-avatar" href="./myblog.php?author=<?php echo $username;?>&page=1">
                        <img width="64" hieght="60" src="../<?php echo $savepath;?>">
                    </a>
                    <div class="user-name">
                        <h5><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $username;?></a></h5>
                        <span><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $email;?></a></span>
                    </div>
                    <div class="btn-group" style="float: right;top: 10px;">
                        <a class="btn mini btn-success" href="../userinfo.php">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                </div>
                <ul class="inbox-nav inbox-divider">
                    <li class="active">
                        <a href="./blog.php?page=1"><i class="fa fa-inbox"></i> 全部博客 </a>
                    </li>
                    <li class="active">
                        <a href="./myblog.php?author=<?php echo $username;?>&page=1"><i class="fa fa-inbox"></i> 查看我的博客 </a>
                    </li>
                    <li class="active">
                        <a href="./temp.php?author=<?php echo $username;?>&page=1"><i class="fa fa-inbox"></i> 查看我的草稿 </a>
                    </li>
                    <li class="active">
                        <a href="./index_editor.php"><i class="fa fa-inbox"></i> 发布博客 </a>
                    </li>
                </ul>
                <div class="blank"> </div>
                <div class="inbox-body text-center">
                    <div class="btn-group">
                        <a class="btn btn-lg btn-danger" href="javascript:;">
                            退出
                        </a>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                    <ul class="nav nav-pills" style="position: absolute;">
                        <li><a style="color:inherit;" href="../admin.php">新闻</a></li>
                        <li><a style="color:inherit;" href="./blog.php?page=1">博客</a></li>
                    </ul>
                    <form action="#" class="pull-right position">
                        <div class="input-append">
                            <input type="text" class="sr-input" placeholder="Search Blog">
                            <button class="btn sr-btn" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="inbox-body col-lg-10 col-lg-offset-1">
                    <div id="editormd-view" style="">
                        <textarea style="display:none;"
                            id="view-editormd-markdown-doc"><?php echo $content==''? '## 还没有数据哦！':$content;?></textarea>
                    </div>
                    <?php
                        if($judgetemp==1){?>
                            <form>
                            <div>
                                <p class="small">评论区</p>
                                <input type="text" class="hidden" name="article_id" value="<?php echo $id;?>">
                                <textarea class="form-control" rows="3" name="content"></textarea>
                                <div class="text-right"><button type="submit" class="btn btn-primary btn-xs"
                                    style="margin-top: 8px; margin-bottom: 15px;" name="comment" formaction="./preserve_comment.php?type=user" 
                                    formmethod="POST">提交评论</button></div>
                            </div>
                            </form>
                            <div><p id="content" class="small">
                                <?php
                                    function myresult($result,$row,$type){
                                        mysqli_data_seek($result, $row);
                                        $row = mysqli_fetch_array($result);
                                        return $row[$type];
                                    }
                                    function selectinfo($stuNum,$type){
                                        require './conn.php';
                                        $sql="select * from infoset where stuNum=$stuNum";
                                        $result=mysqli_query($conn,$sql)
                                            or die("quering error");
                                        $row=mysqli_fetch_array($result);
                                        return $row[$type];
                                    }
                                    for($i = 0; $i < mysqli_num_rows($comment_res); $i++)
                                    {
                                        $create_time = myresult($comment_res, $i, 'create_time');
                                        $comm_content = myresult($comment_res, $i, 'content');
                                        $user_id = myresult($comment_res,$i,'user_id');
                                        $username = selectinfo($user_id,'username');
                                        $savepath = selectinfo($user_id,'save_path');
                                        ?>
                                        <div class="list-item">
                                            <div class="user-face">
                                                <a href="./myblog.php?author=<?php echo $username;?>&page=1" target="_blank">
                                                    <div class="bili-avatar">
                                                        <img class="bili-avatar-img bili-avatar-face" src="../<?php echo $savepath;?>" alt="" width="64" hieght="60">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="con ">
                                                <div class="user">
                                                    <a href="./myblog.php?author=<?php echo $username;?>&page=1" class="name"><?php echo $username;?></a>
                                                </div>
                                                <p class="text">
                                                    <?php echo $comm_content;?>
                                                </p>
                                                <div class="info">
                                                    <span class="time"><?php echo $create_time;?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                ?>
                            </p></div>
                    <?php 
                        }?>
                </div>

            </aside>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            editormd.markdownToHTML("editormd-view", {
                htmlDecode: "style,script,iframe", // you can filter tags decode
                emoji: true,
                taskList: true,
                tex: true, // 默认不解析
                flowChart: false, // 默认不解析
                sequenceDiagram: true, // 默认不解析
                path: "./md/lib/",
                previewTheme: "dark"
            });
        });
    </script>
</body>

</html>