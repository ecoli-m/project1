-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `judgetemp` int(2) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL COMMENT '标题',
  `author` varchar(50) NOT NULL COMMENT '作者',
  `content` longtext NOT NULL COMMENT '文章的内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `article` (`judgetemp`, `id`, `title`, `author`, `content`) VALUES
(1,	3,	'test1',	'Straho',	'asd\r\nasdf\r\nas\r\ndfg\r\nadfg\r\nsd\r\ngfh\r\ndf\r\nsgh\r\nasfd\r\nga\r\nwsdft\r\nawer\r\ngy\r\nd\r\n![å›¾ç‰‡alt](images//1629445669b165.png \'å›¾ç‰‡title\')'),
(1,	4,	'test2',	'Straho',	'asffgdfgh\r\nsfdgå²çš„æ³•å›½\r\n\r\ndfsgsdå‡¤å‡°ç”·\r\nthdsfgä½ \r\nfghdå‘\r\n\r\nsdfgsdfh\r\n\r\nsdfg\r\n\r\nd\r\nghfbdfxg\r\nnb\r\n![å›¾ç‰‡alt](images//1629445755d0ae.png \'å›¾ç‰‡title\')'),
(0,	5,	'test3',	'Straho',	'asd\r\nasd\r\nas\r\nd\r\nas\r\nfsd\r\ngs\r\ndf'),
(1,	7,	'test4asd',	'Straho',	'asdasdas\r\nda\r\nsd\r\nas\r\nd\r\nasd\r\na\r\nsd\r\nas\r\nd\r\nas\r\nasdf\r\na\r\nsdf\r\nasdf\r\na\r\nsdf\r\n'),
(0,	10,	'test7lllllaaaa',	'Straho',	'afgasdg\r\nasd\r\nf\r\nasd\r\nfas\r\ndf\r\nasd\r\nfa\r\nsdf\r\nasdf\r\n');

DROP TABLE IF EXISTS `infoset`;
CREATE TABLE `infoset` (
  `stuNum` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `realname` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Year_Of_Admission` int(10) NOT NULL,
  `department` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `save_path` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `introduction` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `infoset` (`stuNum`, `username`, `realname`, `email`, `Year_Of_Admission`, `department`, `save_path`, `introduction`) VALUES
('200111113',	'Straho',	'é™ˆä½³è±ª',	'Straho@163.com',	2020,	'è®¡ç®—æœºç§‘å­¦ä¸ŽæŠ€æœ¯',	'uploadimg/20210827/16300367887b9c.png',	'æ‰“ç®—å¤§è‹æ‰“'),
('2020',	'å•¦å•¦å•¦å•¦å•¦',	'å¤§è‹æ‰“',	'cjh@163.com',	2020,	'è®¡ç§‘',	'uploadimg/20210824/162980094484c0.png',	'å•Šå®žæ‰“å®žçš„');

DROP TABLE IF EXISTS `signin`;
CREATE TABLE `signin` (
  `stuNum` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `admin` int(30) NOT NULL DEFAULT '0' COMMENT '管理员',
  `secretword` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `signin` (`stuNum`, `admin`, `secretword`, `username`) VALUES
('200111113',	1,	'5610a7c6236ecbdda67c3ca8c75ef3f64f147a89',	'Straho'),
('2020',	0,	'5610a7c6236ecbdda67c3ca8c75ef3f64f147a89',	'cjh');

-- 2021-08-28 14:08:46
