
<?php
 //引入自动加载文件
 require 'D:/phpstudy_pro/WWW/project1/vendor/autoload.php';

use QL\QueryList;


$ql1 = QueryList::get('http://www.chinanews.com/society.shtml');
$category='sh';

//第一级采集
$text = $ql1
    ->rules([
        'url'=>['.dd_bt>a','href'],
    ])
    ->range('.content_list>ul>li')
    ->query()
    ->getdata(function($item){
        return "http://www.chinanews.com".$item['url'];
});


$i=0;
while(!empty($text[$i])){
    $url =$text[$i] ;
        $ql = QueryList::get($url);
        $data = $ql
        ->rules([
            'title'=>['.content>h1','text'],
            'post_info'=>['.content>.left-time>.left-t','html','-a'],
            'post_body'=>['.content>.left_zw','html','-table -div'],
        ])
        ->query()
        ->getdata();
        
        //引入数据库配置
        require_once('conn.php');

        //查询数据库是否以含有该文件
        $dbc = mysqli_connect(HOST,USER,PASS,DBN)
            or die ("connected error");
        $query = "SELECT * FROM `wynews` WHERE url='$url'";
        $result = mysqli_query($dbc,$query)
            or die ("quering error");
        $row = mysqli_fetch_array($result);
        //获取当期日期时间
        $datetime = date("Y-m-d H:i:s");

        //不含该文件则添加
        if(empty($row)&&!empty($data['title'])){
            $query2 = "INSERT INTO `wynews`(`title`, `post_info`, `url`,`post_data`,`path`,`category`) 
                    VALUES ('$data[title]','$data[post_info]','$url','$datetime','','$category')";
            $result = mysqli_query($dbc,$query2)
                    or die ("quering error");
    
            //获取id,生成文件路径
            $query3 = "SELECT `id` FROM `wynews` WHERE url='$url'";
            $res = mysqli_query($dbc,$query3)
                or die ("quering error");
            $id = mysqli_fetch_array($res);
            $id = $id[0];
            $file_path = "../news/".$id.".html";
            $fp="D:/phpstudy_pro/WWW/project1/news/news/".$id.".html";
            //写入文件
            $body=str_replace(array('class="f_center"','<p>'),['style="text-align:center;"','<p style="text-indent:2em;" >'],$data[post_body]);
            file_put_contents($fp,$body);

            //将路径存入数据库
            $query3 = "UPDATE `wynews` SET path ='$file_path' WHERE url='$url'";
            $result2 = mysqli_query($dbc,$query3)
                    or die ("quering error");
        }
    $i++;
        }       
?>