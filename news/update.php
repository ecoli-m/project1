<?php
    $type=$_GET['type'];
    $id=$_GET['id'];
    $author=$_GET['author'];
    $page=$_GET['page'];

    require './conn.php';
    $sql="select * from article where id=$id";
    $rt=mysqli_query($conn,$sql) or die("get quering error");
    $row=mysqli_fetch_array($rt);

    $title=$row['title'];
    $precontent=$row['content'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Editor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" >
    <link rel="stylesheet" type="text/css" href="./md/css/editormd.css" />
    <link rel="stylesheet" type="text/css" href="./md/css/editormd.preview.css"/>

    <script src="./js/jquery.min.js"></script>
    <script src="./md/editormd.js"></script>
    <script src="./md/lib/marked.min.js"></script>
    <script src="./md/lib/prettify.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" ></script> 
</head>

<body>
    <?php
        require './getinfo.php';
    ?>
    <div class="container-fluid">
        <link rel='stylesheet prefetch'
            href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                    <a class="inbox-avatar" href="./myblog.php?author=<?php echo $username;?>&page=1">
                        <img width="64" hieght="60" src="../<?php echo $savepath;?>">
                    </a>
                    <div class="user-name">
                        <h5><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $username;?></a></h5>
                        <span><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $email;?></a></span>
                    </div>
                    <div class="btn-group" style="float: right;top: 10px;">
                        <a class="btn mini btn-success" href="../userinfo.php">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                </div>
                <ul class="inbox-nav inbox-divider">
                    <li class="active">
                        <a href="./blog.php?page=1"><i class="fa fa-inbox"></i> 全部博客 </a>
                    </li>
                    <li class="active">
                        <a href="./myblog.php?author=<?php echo $username;?>&page=1"><i class="fa fa-inbox"></i> 查看我的博客 </a>
                    </li>
                    <li class="active">
                        <a href="./temp.php?author=<?php echo $username;?>&page=1"><i class="fa fa-inbox"></i> 查看我的草稿 </a>
                    </li>
                    <li class="active">
                        <a href="./index_editor.php"><i class="fa fa-inbox"></i> 发布博客 </a>
                    </li>
                </ul>
                <div class="blank"> </div>
                <div class="inbox-body text-center">
                    <div class="btn-group">
                        <a class="btn btn-lg btn-danger" href="../logout.php">
                            退出
                        </a>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                    <ul class="nav nav-pills" style="position: absolute;">
                        <li><a style="color:inherit;" href="../admin.php">新闻</a></li>
                        <li><a style="color:inherit;" href="./blog.php?page=1">博客</a></li>
                    </ul>
                    <form action="#" class="pull-right position">
                        <div class="input-append">
                            <input type="text" class="sr-input" placeholder="Search Blog">
                            <button class="btn sr-btn" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="inbox-body">
                    <div class="row">
                        <form action="./updatesql.php" method="POST">
                            <input type="text" class="hidden" name="id" value="<?php echo $id;?>">
                            <input type="text" class="hidden" name="author" value="<?php echo $author;?>">
                            <div id="title">
                                <input type="text" name="title" style="width: 100%;" value="<?php echo $title;?>">
                            </div>
                            <div id="content">
                                <textarea name="content-markdown-doc" style="display:none;"><?php echo $precontent;?></textarea>
                            </div>
                            <div class="inbox-body text-center">
                                <div class="btn-group">
                                    <input class="btn btn-lg btn-danger" type="submit" name="editor" value="提交并发布"/>
                                </div>
                                -----------------------------
                                <div class="btn-group">
                                    <input class="btn btn-lg btn-danger" type="submit" name="editor_temp" value="保存到草稿"/>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>
            </aside>
        </div>
    </div>
</body>

<script src="./md/plugins/editormd-image-past.js"></script>
<script type="text/javascript">
    $(function() {
        var editor = editormd("content", {
            width: "100%",
            height: 680,
            path : './md/lib/',
            theme : "light",
            previewTheme : "light",
            editorTheme : "pastel-on-light",
            // markdown : md,
            codeFold : true,
            syncScrolling : "single",
            watch : true,
            saveHTMLToTextarea : true,    // 注意3：这个配置，方便post提交表单，保存 HTML 到 Textarea
            searchReplace : true,
            //watch : false,                // 关闭实时预览
            htmlDecode : "style,script,iframe|on*",            // 开启 HTML 标签解析，为了安全性，默认不开启
            //toolbar  : false,             //关闭工具栏
            //previewCodeHighlight : false, // 关闭预览 HTML 的代码块高亮，默认开启
            emoji : true,
            taskList : true,
            tocm            : true,         // Using [TOCM]
            tex : true,                   // 开启科学公式TeX语言支持，默认关闭
            flowChart : true,             // 开启流程图支持，默认关闭
            sequenceDiagram : true,       // 开启时序/序列图支持，默认关闭,
            //dialogLockScreen : false,   // 设置弹出层对话框不锁屏，全局通用，默认为true
            //dialogShowMask : false,     // 设置弹出层对话框显示透明遮罩层，全局通用，默认为true
            //dialogDraggable : false,    // 设置弹出层对话框不可拖动，全局通用，默认为true
            //dialogMaskOpacity : 0.4,    // 设置透明遮罩层的透明度，全局通用，默认值为0.1
            //dialogMaskBgColor : "#000", // 设置透明遮罩层的背景颜色，全局通用，默认为#fff
            imageUpload : true,
            imageFormats : ["jpg", "jpeg", "gif", "png", "bmp", "webp","zip","rar"],
            imageUploadURL : "./upload.php",
            crossDomainUpload: false,   // 是否启用跨域上传
            
            onload : function() {
                console.log('onload', this);
                initPasteDragImg(this); //必须 引入图片复制和截图上传功能
                //this.fullscreen();
                //this.unwatch();
                //this.watch().fullscreen();

                //this.setMarkdown("#PHP");
                //this.width("100%");
                //this.height(480);
                //this.resize("100%", 640);
            }
        });
    });
</script>

</html>