<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Responsive Mail Inbox and Compose - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/blog.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
<?php $category=$_GET['category'];
      $page=$_GET['page'];
      require 'getinfo.php';
?>
    <div class="container-fluid">
        <link rel='stylesheet prefetch'
            href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                    <a class="inbox-avatar" href="../userinfo.php">
                        <img width="64" hieght="60" src="../<?php echo $savepath;?>">
                    </a>
                    <div class="user-name">
                        <h5><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $username;?></a></h5>
                        <span><a href="./myblog.php?author=<?php echo $username;?>&page=1"><?php echo $email;?></a></span>
                    </div>
                    <div class="btn-group" style="float: right;top: 10px;">
                        <a class="btn mini btn-success" href="../userinfo.php">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                </div>
                <ul class="inbox-nav inbox-divider">
                    <li class="active">
                        <a href="showall.php?category=&page=1"><i class="fa fa-inbox"></i> 全部 </a>
                    </li>
                    <li>
                        <a href="showall.php?category=ty&page=1"><i class="fa fa-bookmark-o"></i> 体育</a>
                    </li>
                    <li>
                        <a href="showall.php?category=cj&page=1"><i class="fa fa-bookmark-o"></i> 财经</a>
                    </li>
                    <li>
                        <a href="showall.php?category=gj&page=1"><i class="fa fa-bookmark-o"></i> 国际</a>
                    </li>
                    <li>
                        <a href="showall.php?category=sh&page=1"><i class=" fa fa-trash-o"></i> 社会</a>
                    </li>
                </ul>
                <div class="blank"> </div>
                <div class="inbox-body text-center">
                    <div class="btn-group">
                        <a class="btn btn-lg btn-danger" href="../logout.php">
                            退出
                        </a>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                  <ul class="nav navbar-nav">
                        <li><a style="color:inherit;" href="../admin.php">新闻</a></li>
                        <li><a style="color:inherit;" href="./blog.php?page=1">博客</a></li>
                  </ul>
                    <form action="getnews/search.php" class="pull-right position">
                        <div class="input-append">
                            <input type="text" class="sr-input" placeholder="Search Blog" id = "search" name = "search">
                            <button class="btn sr-btn" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <table class="table table-inbox table-hover">
                      <tbody>
                      <?php
                        require 'is_admin.php';

                          require_once('getnews/conn.php');
                          $startpage=($page-1)*15;
                          
                          $dbc = mysqli_connect(HOST,USER,PASS,DBN)
                              or die ("connected error"); 
                          
                          if(empty($category)){
                            $query = "SELECT * FROM `wynews`  order by id desc LIMIT $startpage,15";
                            $Query2 = "SELECT count(*) as AllNum from `wynews`";
                            }
                          else{
                            $query = "SELECT * FROM `wynews` WHERE category='$category' order by id desc LIMIT $startpage,15";
                            $Query2 = "SELECT count(*) as AllNum from `wynews` WHERE category='$category'";
                            }
                          
                          $result = mysqli_query($dbc,$query)
                              or die ("quering error");
                              
                          
                          $result2  = mysqli_query($dbc,$Query2);
                          $result3= mysqli_fetch_assoc($result2);  

                          while ($row = mysqli_fetch_array($result)){ $link = "news/demo.php?id=".$row['id'];?>
                        <tr class="container">
                            <td class="view-message col-lg-6"><a style="color:inherit;" href="<?php echo $link?>"><?php echo $row['title']?></td>
                            <td class="view-message  dont-show"><?php echo $row['category']?></td>
                            <td></td>
                            <td class="view-message   infomation">阅读(10)评论(10)收藏(10)</td>
                            <td class="view-message  col-lg-offset-12 text-right"><?php echo $row['post_data']?></td>
                            <?php
                                if($is_admin){
                                    ?>
                                    <td class="inbox-small-cells">
                                        <a href="./delete.php?type=news&id=<?php echo $row['id'];?>&category=<?php echo $row['category'];?>&page=<?php echo $page;?>" onClick="return confirm('确定删除?');" class="btn btn-default btn-xs">删除</a>
                                    </td>
                                    <?php
                                }
                            ?>
                        </tr>
                      <?php }?>    
                    </tbody>
                    </table>
                    <?php 
                          $shownum = 15;
                          $pageset = 15;
                          $totalpages = $result3['AllNum'];
                          $paging = (int)($totalpages/$pageset)+1;
                    ?>
                    <div class="text-center"><ul class="pagination pagination-large">
                      <?php if ($page!=1){$page -= 1;?>
                          <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$page;?>" rel="prev">«</a></li>
                      <?php $page+=1;}else{?>
                          <li class="disabled"><span>«</span></li>
                      <?php } ?>
                      
                      <?php  
                      if ($paging<=$pageset){
                          for ($i=1;$i<$page;$i++){?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php } ?>
                          <li class='active'><span><?php echo $page; ?></span></li>
                          <?php
                          for ($i=$page+1;$i<=$paging;$i++){?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php } }
                      else{ if ($page<8){ for ($i=1;$i<$page;$i++){?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php }}else{ for ($i=1;$i<=3;$i++){ ?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php }?>
                              <li class="disabled"><span>...</span></li>
                          <?php for ($i=3;$i>0;$i--){ $a=$page-$i;?>
                              <li ><a href="<?php echo 'showall.php?category='.$category.'&page='.$a;?>"><?php echo $a; ?></a></li>
                          <?php }}?>
                              <li class='active'><span><?php echo $page; ?></span></li>
                          <?php if ($page+7>$paging){for ($i=$page+1;$i<=$paging;$i++){ ?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php }}else{ for ($i=$page+1;$i<=$page+3;$i++){ ?>
                              <li><a href="<?php echo 'showall.php?category='.$category.'&page='.$i;?>"><?php echo $i; ?></a></li>
                          <?php }?>
                              <li class="disabled"><span>...</span></li>
                          <?php for ($i=2;$i>=0;$i--){ $a=$paging-$i;?>
                              <li ><a href="<?php echo 'showall.php?category='.$category.'&page='.$a;?>"><?php echo $a; ?></a></li>
                          <?php }}}?>

                      <?php if ($page!=$paging){ $page += 1 ;?>
                          <li ><a href="<?php echo 'showall.php?category='.$category.'&page='.$page;?>">»</a></li>
                      <?php }else{?>
                          <li class="disabled"><span>»</span></li>
                      <?php }?>

                      </ul>
                  </div>
            </aside>
        </div>
    </div>
    <script type="text/javascript">

    </script>
</body>

</html>