-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `news_comment`;
CREATE TABLE `news_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(20) unsigned NOT NULL,
  `user_id` int(20) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `news_comment` (`id`, `article_id`, `user_id`, `content`, `create_time`) VALUES
(1,	1717,	200111113,	'adsfasdf',	'2021-08-29 14:20:30'),
(2,	1717,	200111113,	'adsfasdf',	'2021-08-29 14:23:03'),
(3,	1945,	200111113,	'asdasdasd',	'2021-08-29 14:23:22'),
(4,	1717,	200111113,	'asd',	'2021-08-29 14:32:01'),
(5,	1717,	200111113,	'llllll',	'2021-08-29 14:36:09'),
(6,	1945,	200111113,	'llllllllll',	'2021-08-29 14:39:05'),
(7,	1945,	200111113,	'å•¦å•¦å•¦',	'2021-08-29 14:41:48');

DROP TABLE IF EXISTS `user_comment`;
CREATE TABLE `user_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(20) unsigned NOT NULL,
  `user_id` int(20) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户评论';

INSERT INTO `user_comment` (`id`, `article_id`, `user_id`, `content`, `create_time`) VALUES
(4,	4,	200111113,	'é˜¿æ–¯è’‚èŠ¬å™¶å£«å¤§å¤«',	'2021-08-22 17:02:53'),
(12,	4,	200111113,	'å¤§å¸ˆå‚…æ•¢æ­»é˜Ÿé£Žæ ¼ä¸œæ–¹ä¸è´¥',	'2021-08-27 11:38:34'),
(7,	5,	200111113,	'dasfasdf',	'2021-08-22 17:14:24'),
(8,	5,	200111113,	'asdfasdfasdfasdfasdfasdfasdfasdfasdf',	'2021-08-22 17:15:22'),
(11,	4,	200111113,	'åœ¨è¥¿æ–¹æ’’æ—¦v',	'2021-08-27 11:37:57'),
(13,	5,	200111113,	'å•Šæ‰“å‘æ’’æ—¦',	'2021-08-27 11:39:31'),
(14,	4,	200111113,	'å•Šå®žæ‰“å®žçš„',	'2021-08-27 12:00:02'),
(15,	4,	200111113,	'å•Šå¤§è‹æ‰“',	'2021-08-27 13:21:41'),
(16,	4,	200111113,	'å•Šå®žæ‰“å®žå¤§è‹æ‰“',	'2021-08-27 13:21:44'),
(17,	4,	200111113,	'é˜¿ä¸‰å¤§è‹æ‰“å®žæ‰“å®žçš„',	'2021-08-27 13:21:47'),
(18,	4,	200111113,	'å•Šå®žæ‰“å®žå¤§è‹æ‰“',	'2021-08-27 13:21:51'),
(19,	4,	200111113,	'å•Šå®žæ‰“å®žå¤§è‹æ‰“å®žæ‰“å®žå¤§è‹æ‰“',	'2021-08-27 13:21:55'),
(20,	4,	200111113,	'å®‰æŠšå“ˆçœ‹ä¼¼ç®€å•',	'2021-08-27 13:22:09'),
(21,	4,	200111113,	'çš„åˆ†å¸ƒå¼çš„å¦‡å¥³ä¸å¡å°±æœä½ ',	'2021-08-27 13:22:15'),
(22,	4,	200111113,	'å•Šé¥¿å•Šå®˜æ–¹',	'2021-08-27 13:22:19'),
(23,	3,	200111113,	'asdasd',	'2021-08-27 21:52:08'),
(24,	3,	200111113,	'ZXCds',	'2021-08-28 20:05:40'),
(26,	3,	200111113,	'ASD',	'2021-08-29 14:31:40');

-- 2021-08-29 06:53:01
