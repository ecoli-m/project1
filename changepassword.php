<?php
    session_start();
    function str_check($str){
        if(!preg_match("/^[A-Za-z0-9]+$/",$str)){
            return TRUE;
        }
        else 
            return FALSE;
    }
    if(isset($_POST['submit'])){
        $username=$_POST["username"];
        $oldpassword=$_POST["oldpassword"];
        $newpassword=$_POST["newpassword"];
        $assertpassword=$_POST["assertpassword"];
        $feedback="修改失败";
        if(empty($username)||empty($oldpassword)||empty($newpassword)||empty($assertpassword)){
            $feedback="请完整填写表单";
        }
        elseif(str_check($username)){
            $feedback="用户名请使用请使用英文或数字";
        }
        elseif($newpassword!=$assertpassword){
            $feedback="两次输入的密码不相同";
        }
        elseif(!preg_match("/^(?![^a-zA-Z]+$)(?!\D+$).{6,16}$/",$newpassword)){
            $feedback="您的密码必须含有英文和数字，不能有特殊字符，长度6到16个字符";
        }
        else{
            //引入数据库参数
            require_once('conn.php');
            $conn=mysqli_connect(HOST,USER,PASS,DBN)
                or die("数据库链接失败");
            $result=mysqli_query($conn,"SELECT * From signin Where username='{$username}'");
            $oldpassword=sha1($oldpassword);
            $newpassword=sha1($newpassword);
            $dbusername=null;
            $dbpassword=null;
            $judge=1;
            while( $row = mysqli_fetch_array($result,MYSQLI_BOTH)){
                $dbusername=$row['username'];
                $dbpassword=$row['secretword'];
            }
            if(is_null($dbusername)||$username!=$dbusername){
                $judge=0;
                $feedback="用户名不存在";
            }
            if($oldpassword!=$dbpassword){
                $judge=0;
                $feedback="密码错误";
            }
            if($judge==1){
                mysqli_query($conn,"update signin set secretword ='{$newpassword}' where username='{$username}'")
                    or die("存入数据库失败");
                mysqli_close($conn);
                header("Location: login.php");
            }
        }
    }
    else{
        $feedback="";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/changepassword.css" type="text/css">
    <link rel="Shortcut Icon" href="img/favicon.ico" type="image/x-icon" />
    <title>ChangePassword</title>
</head>
<body>
    <div class="layout">
        <main>
            <div id="reg" data-v-ff518656="">
                <!--背景-->
                <div class="reg-bg" data-v-ff518656="">
    
                </div> 
                <div class="reg-container" data-v-ff518656="">
                    <div class="reg-wrapper" data-v-ff518656="">
                        <!--表格页眉-->
                        <div class="reg-head" data-v-ff518656="">
                            <!--***这里是logo图片位置***-->
                            <img src="img/hitsz.jpeg" class="reg-logo" data-v-ff518656="">
                            <!--表格名称--> 
                            <span class="reg-title" data-v-ff518656="">修改密码</span>
                        </div>
                        <!--填表清单-->
                        <div class="reg-form-wrapper" data-v-ff518656=""> 
                            <div id="changecheck">
                                <?php echo '<p>'.$feedback.'</p>' ;?>
                            </div>
                            <form class="reg-form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                                
                                <!--单个输入元素--> 
                                <div class=" form-item">
                                    <!--输入元素样式-->
                                    <span class="reg-input input-wrapper input-group input-group-prepend">
                                        <!--***图标***-->
                                        <div class="input-prepend">
                                            <!--图片宽31高41-->
                                            <img src="img/username.PNG">
                                        </div> 
                                        <!--***输入框调整提示语和输入类型***-->
                                        <input placeholder="请输入用户名" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="username" name="username"> 
                                    </span> 
                                </div> 
                                <!--单个输入元素--> 
                                <div class=" form-item">
                                    <!--输入元素样式-->
                                    <span class="reg-input input-wrapper input-group input-group-prepend">
                                        <!--***图标***-->
                                        <div class="input-prepend">
                                            <!--图片宽31高41-->
                                            <img src="img/password.PNG">
                                        </div> 
                                        <!--***输入框调整提示语和输入类型***-->
                                        <input placeholder="请输入原密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="oldpassword" name="oldpassword"> 
                                    </span> 
                                </div> 
                                <!--单个输入元素--> 
                                <div class=" form-item">
                                    <!--输入元素样式-->
                                    <span class="reg-input input-wrapper input-group input-group-prepend">
                                        <!--***图标***-->
                                        <div class="input-prepend">
                                            <!--图片宽31高41-->
                                            <img src="img/password.PNG">
                                        </div> 
                                        <!--***输入框调整提示语和输入类型***-->
                                        <input placeholder="请输入新密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="newpassword" name="newpassword"> 
                                    </span> 
                                </div>
                                <!--单个输入元素--> 
                                <div class=" form-item">
                                    <!--输入元素样式-->
                                    <span class="reg-input input-wrapper input-group input-group-prepend">
                                        <!--***图标***-->
                                        <div class="input-prepend">
                                            <!--图片宽31高41-->
                                            <img src="img/password.PNG">
                                        </div> 
                                        <!--***输入框调整提示语和输入类型***-->
                                        <input placeholder="请确认新密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="assertpassword" name="assertpassword"> 
                                    </span> 
                                </div>
                                <!--***提交按钮***-->
                                <button type="submit" value="修改密码" name="submit" class="reg-submit">
                                    修改密码
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>