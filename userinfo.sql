-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2021-08-16 00:57:04
-- 服务器版本： 8.0.12
-- PHP 版本： 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `userinfo`
--

-- --------------------------------------------------------

--
-- 表的结构 `infoset`
--

CREATE TABLE `infoset` (
  `realname` varchar(30) COLLATE utf8_unicode_ci NOT NULL COMMENT '真实姓名',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '电子邮箱',
  `year_of_admission` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '入学年份',
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '专业',
  `save_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '存储路径',
  `introduction` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '简介'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `signin`
--

CREATE TABLE `signin` (
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `secretword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `stuNum` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` int(30) DEFAULT '0' COMMENT '管理员'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `signin`
--

INSERT INTO `signin` (`username`, `secretword`, `stuNum`, `admin`) VALUES
('admin', '2e12846c2d42d9556da25fb87056eb5b2da0efd6', '200111529', 0),
('ecoli', '54761d3f56f0c76bd0db327c4978c8772d71df7d', '200111528', 0),
('mxy', '54761d3f56f0c76bd0db327c4978c8772d71df7d', '200111529', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
