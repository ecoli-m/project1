<?php
    error_reporting(E_ALL || ~E_NOTICE); 
    $stuNum=$_COOKIE['stuNum'];
    require_once("conn.php");
    //建立链接
    $conn=mysqli_connect(HOST,USER,PASS,DBN)
            or die("connect failed");
    //$ql = "INSERT INTO realname,email,Year_Of_Admission,department,save_path,introduction FROM infoset";
    //$res = mysqli_query($conn,$sql)
      //      or die("查询失败");
    //$rn = $res[]
    //处理用户设置信息
    if(isset($_POST['setting'])){
        //接受用户提交的信息
        $username=trim($_POST['username']);
        $realname=trim($_POST['realname']);
        $email=trim($_POST['email']);
        $YearOfAdmission=trim($_POST['YearOfAdmission']);
        $department=trim($_POST['department']);
        $introduction=trim($_POST['introduction']);
        //图片处理
        $file = $_FILES['up_load_picture'];
        $ext  = getExt($file['name']);
        $dir_name = 'uploadimg/'.date('Ymd');
        createDirectory($dir_name); 
        $file_name  = $dir_name.'/'.time().substr(uniqid(),-4).$ext;
        move_uploaded_file($file['tmp_name'], $file_name);
        $savepath=$file_name;
        //将用户设置的信息插入（或更新）数据库
        $result=mysqli_query($conn,"SELECT * From infoset Where stuNum='{$stuNum}'");
        $row = mysqli_fetch_array($result,MYSQLI_BOTH);
        if(is_null($row['stuNum'])){
            $sql="INSERT INTO infoset (stuNum,username,realname,email,Year_Of_Admission,department,save_path,introduction)
                VALUE('$stuNum','$username','$realname','$email','$YearOfAdmission','$department','$savepath','$introduction')";
            $result = mysqli_query($conn,$sql)
                or die("query failed");
        }
        else{
            $sql="update infoset set username='{$username}', realname='{$realname}', email='{$email}', Year_Of_Admission='{$YearOfAdmission}',
            department='{$department}', save_path='{$savepath}', introduction='{$introduction}' where stuNum='{$stuNum}'";
            $result = mysqli_query($conn,$sql)
                or die("infoset query failed");
            $sql = "update signin set username='{$username}' where stuNum='{$stuNum}'";
            $result = mysqli_query($conn,$sql)
                or die("signin query failed");
        }
        header("Location: userinfo.php");
    }
    function createDirectory($dir)
    {
        //$dir = iconv("UTF-8","gb2312", $dir );
        return  is_dir ( $dir ) or createDirectory(dirname($dir)) and  mkdir ( $dir , 0777);
    }
    function getExt($filename)
    {
    $pos = strrpos($filename, '.');
    $ext = substr($filename, $pos);
    return $ext;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/setting.css" rel="stylesheet" id="bootstrap-css">
    <!------ Include the above in your HEAD tag ---------->
    <title>Setting</title>
</head>


<body>
    <form action="./setting.php" enctype="multipart/form-data" method="POST">
    <div class="main-content">
        <div class="container mt-7">
            <!-- Table -->
            <div class="row">
                <div class="col-xl-8 m-auto order-xl-1">
                    <div class="card bg-secondary shadow">
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">My account</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="./changepassword.php" class="btn btn-sm btn-primary">修改密码</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form>
                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">学号</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $stuNum; ?>" name="stuNum" realonly="true">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">用户名</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    placeholder="Username" name="username">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label">真实姓名</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    placeholder="Realname" name="realname">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">邮箱</label>
                                                <input type="email" class="form-control form-control-alternative"
                                                    placeholder="...@example.com" name="email">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">入学年份</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    placeholder="Year of admission" name="YearOfAdmission">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">院系</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    placeholder="Department" name="department">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Address -->
                                <h6 class="heading-small text-muted mb-4">PERSONALIZATION</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group focused">
                                                <label class="form-control-label">头像设置</label>
                                                <label class="btn btn-sm fileContainer">
                                                    上传文件
                                                    <input enctype="multipart/form-data" type="file" name="up_load_picture">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Description -->
                                <h6 class="heading-small text-muted mb-4">About me</h6>
                                <div class="pl-lg-4">
                                    <div class="form-group focused">
                                        <label class="form-control-label">个人简介</label>
                                        <textarea rows="4" class="form-control form-control-alternative"
                                            placeholder="A few words about you ..." name="introduction"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <input type="submit" name="setting" value="提交" class="submit btn btn-sm btn-primary">
                </div>
                
            </div>
            
        </div>
        
    </div>
    </form>
</body>

</html>