<?php
    require_once("conn.php");
    //连接数据库
    $conn=mysqli_connect(HOST,USER,PASS,DBN)
            or die("connect failed");
    $stuNum=$_COOKIE['stuNum'];
    $result=mysqli_query($conn,"SELECT * From infoset Where stuNum='{$stuNum}'");
    $row = mysqli_fetch_array($result,MYSQLI_BOTH);
    if(is_null($row['stuNum'])){
        header("Location: setting.php");
    }
    else{
        $username=$row['username'];
        $realname=$row['realname'];
        $email=$row['email'];
        $YearOfAdmission=$row['Year_Of_Admission'];
        $department=$row['department'];
        $savepath=$row['save_path'];
        $introduction=$row['introduction'];
        mysqli_close($conn);
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/setting.css" rel="stylesheet" id="bootstrap-css">
    <!------ Include the above in your HEAD tag ---------->
    <title>UserInfo</title>
</head>


<body>
    <div class="main-content">
        <div class="container mt-7">
            <!-- Table -->
            <div class="row">
                <div class="col-xl-8 m-auto order-xl-1">
                    <div class="card bg-secondary shadow">
                    <a href="./admin.php" class="btn btn-sm btn-primary">返回主页</a>
                        <div class="card-header bg-white border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">My account</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <a href="./changepassword.php" class="btn btn-sm btn-primary">修改密码</a>
                                    <a href="./setting.php" class="btn btn-sm btn-primary">修改信息</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form>
                                <h6 class="heading-small text-muted mb-4">User information</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">学号</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $stuNum; ?>" name="stuNum" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">用户名</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $username; ?>" name="username" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label">真实姓名</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $realname; ?>" name="realname" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">邮箱</label>
                                                <input type="email" class="form-control form-control-alternative"
                                                    value="<?php echo $email; ?>" name="email" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">入学年份</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $YearOfAdmission; ?>" name="YearOfAdmission" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group focused">
                                                <label class="form-control-label">院系</label>
                                                <input type="text" class="form-control form-control-alternative"
                                                    value="<?php echo $department; ?>" name="department" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Address -->
                                <h6 class="heading-small text-muted mb-4">PERSONALIZATION</h6>
                                <div class="pl-lg-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group focused">
                                                <label class="form-control-label">头像设置</label>
                                                <img width="320" hieght="300" src="<?php echo $savepath; ?>" alt="Your picture" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <!-- Description -->
                                <h6 class="heading-small text-muted mb-4">About me</h6>
                                <div class="pl-lg-4">
                                    <div class="form-group focused">
                                        <label class="form-control-label">个人简介</label>
                                        <textarea rows="4" class="form-control form-control-alternative"
                                        placeholder="<?php echo $introduction; ?>" name="introduction" readonly></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        
    </div>
</body>

</html>