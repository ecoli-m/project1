<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Responsive Mail Inbox and Compose - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="news/css/blog.css">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
        require 'news/getinfo.php';
    ?>
    <div class="container-fluid">
        <link rel='stylesheet prefetch'
            href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
        <div class="mail-box">
            <aside class="sm-side">
                <div class="user-head">
                <?php 
	  header('Content-type:text/html; charset=utf-8');
	  // 开启Session
	  session_start();
 
	  // 首先判断Cookie是否有记住了用户信息
	  if (isset($_COOKIE['stuNum'])) {
		# 若记住了用户信息,则直接传给Session
		$_SESSION['stuNum'] = $_COOKIE['stuNum'];
		$_SESSION['islogin'] = 1;
	  }
	  if (isset($_SESSION['islogin'])) {
        $stuNum=$_COOKIE['stuNum'];
        require 'conn.php';
        $conn=mysqli_connect(HOST,USER,PASS,DBN)
            or die("数据库连接失败");
        $sql="SELECT * from infoset where stuNum='$stuNum'";
        $result=mysqli_query($conn,$sql) or die("1quering error");
        $row=mysqli_fetch_array($result);
        $username=$row['username'];
        $email=$row['email'];
        $savepath=$row['save_path'];
    ?>
		  <a class="inbox-avatar" href="userinfo.php">
                        <img width="64" hieght="60"
                            src="<?php echo $savepath;?>"
                            alt="请完善个人信息">

                    </a>
	  <?php } else { ?>
        <a class="inbox-avatar" href="login.php">
                        <img width="64" hieght="60"
                            src="<?php echo $savepath;?>"
                            alt="请完善个人信息">
                    </a>
    <?php
	  }

      //获取文章

        $dbc = mysqli_connect(HOST,USER,PASS,'news')
            or die ("connected error"); 
        $date = date("Y-m-d");
        $datetime=$date."%";
        $query = "SELECT * FROM `wynews`  order by post_data desc LIMIT 12";

        $result = mysqli_query($dbc,$query)
            or die ("1quering error");
        
    ?>
                    
                    <div class="user-name">

                        <h5><a href="news/myblog.php?author=<?php echo $username;?>&page=1"><?php echo $username;?></a></h5>
                        <span><a href="news/myblog.php?author=<?php echo $username;?>&page=1"><?php echo $email;?></a></span>

                    </div>
                    <div class="btn-group" style="float: right;top: 10px;">
                        <a class="btn mini btn-success" href="userinfo.php">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                </div>
                <ul class="inbox-nav inbox-divider">
                    <li class="active">
                        <a href="news/showall.php?category=&page=1"><i class="fa fa-inbox"></i> 全部 </a>
                    </li>
                    <li>
                        <a href="news/showall.php?category=ty&page=1"><i class="fa fa-bookmark-o"></i> 体育</a>
                    </li>
                    <li>
                        <a href="news/showall.php?category=cj&page=1"><i class="fa fa-bookmark-o"></i> 财经</a>
                    </li>
                    <li>
                        <a href="news/showall.php?category=gj&page=1"><i class="fa fa-bookmark-o"></i> 国际</a>
                    </li>
                    <li>
                        <a href="news/showall.php?category=sh&page=1"><i class=" fa fa-trash-o"></i> 社会</a>
                    </li>
                </ul>
                <div class="blank"> </div>
                <div class="inbox-body text-center">
                    <div class="btn-group">
                        <a class="btn btn-lg btn-danger" href="logout.php">
                            退出
                        </a>
                    </div>
                </div>
            </aside>
            <aside class="lg-side">
                <div class="inbox-head">
                    <ul class="nav nav-pills" style="position: absolute;">
                        <li><a style="color:inherit;" href="./admin.php">新闻</a></li>
                        <li><a style="color:inherit;" href="./news/blog.php?page=1">博客</a></li>
                        <li><a style="color:inherit;" href="">论坛</a></li>
                    </ul>
                    <form action="news/getnews/search.php" class="pull-right position">
                        <div class="input-append">
                            <input type="text" class="sr-input" placeholder="Search Blog" id="seaarch" name ="search">
                            <button class="btn sr-btn" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="inbox-body">
                    <div class="row">
                        <!--
                        <div class="col-lg-6 " style="height: 285px; margin-bottom: 20px;">
                            <div class="thumbnail" style="height: 285px;">
                                <img src="..." alt="..."
                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                <div class="caption">
                                    <h3>标题</h3>
                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容</p>
                                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#"
                                            class="btn btn-default" role="button">Button</a></p>
                                </div>
                            </div>
                        </div>
                        -->
                        <div class="col-lg-6 " style="height: 285px; margin-bottom: 20px;">
                            <div class="container" style="width: auto; padding: 0;">
                                <div id="index_carousel" class="carousel slide" data-ride="carousel"
                                    style="height: 285px;">
                                    <!-- 指示器 Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#index_carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#index_carousel" data-slide-to="1"></li>
                                        <li data-target="#index_carousel" data-slide-to="2"></li>
                                    </ol>

                                    <!-- 轮播展示 
                            * item 表示一个图片，与“指示器”li的个数保持一致 
                            -->
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 左右控制区 Controls 
                            * href 用于确定点击触发的那个轮播图 
                            -->
                                    <a class="left carousel-control" href="#index_carousel" role="button"
                                        data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">前一张</span>
                                    </a>
                                    <a class="right carousel-control" href="#index_carousel" role="button"
                                        data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">下一张</span>
                                    </a>
                                </div>

                            </div>



                        </div>
                        <div class="col-lg-6 " style="height: 285px; margin-bottom: 20px;">
                            <div class="container" style="width: auto; padding: 0;">
                                <div id="index_carousel" class="carousel slide" data-ride="carousel"
                                    style="height: 285px;">
                                    <!-- 指示器 Indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#index_carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#index_carousel" data-slide-to="1"></li>
                                        <li data-target="#index_carousel" data-slide-to="2"></li>
                                    </ol>

                                    <!-- 轮播展示 
                            * item 表示一个图片，与“指示器”li的个数保持一致 
                            -->
                                    <div class="carousel-inner" role="listbox">
                                        <div class="item active">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail" style="height: 285px;">
                                                <img src="..." alt="..."
                                                    style="width: 300px; height: 275px; float: left; margin: 0px 10px 0px 0px;">
                                                <div class="caption">
                                                    <h3>标题</h3>
                                                    <p>这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容这里是文章内容
                                                    </p>
                                                    <p><a href="#" class="btn btn-primary" role="button">Button</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 左右控制区 Controls 
                            * href 用于确定点击触发的那个轮播图 
                            -->
                                    <a class="left carousel-control" href="#index_carousel" role="button"
                                        data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">前一张</span>
                                    </a>
                                    <a class="right carousel-control" href="#index_carousel" role="button"
                                        data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">下一张</span>
                                    </a>
                                </div>

                            </div>



                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4" style="height: 570px; overflow:hidden;">
                            <div class="panel panel-default" style="height: 100.0%;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">今日要闻</h3>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                        <?php while ($row=mysqli_fetch_array($result)){
                                            $link = "news/news/demo.php?id=".$row['id'];?>
                                        <li class="in-panel"><a class="five" href="<?php echo $link?>" target="_blank"><?php echo $row['title']?></a></li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4" style="height:570px;">
                            <div class="panel panel-default" style="height:50%;overflow:hidden;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">体育</h3>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    <?php 
                                            $query1 = "SELECT * FROM `wynews`  where category='ty' order by post_data desc LIMIT 5";

                                            $result1 = mysqli_query($dbc,$query1)
                                                or die ("1quering error");
                                            while ($row1=mysqli_fetch_array($result1)){
                                            $link = "news/news/demo.php?id=".$row1['id'];?>
                                        <li class="in-panel"><a class="five" href="<?php echo $link?>" target="_blank"><?php echo $row1['title']?></a></li><?php }?>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel panel-default" style="height:46%;overflow:hidden;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">财经</h3>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    <?php 
                                            $query1 = "SELECT * FROM `wynews`  where category='cj' order by post_data desc LIMIT 5";

                                            $result1 = mysqli_query($dbc,$query1)
                                                or die ("1quering error");
                                            while ($row1=mysqli_fetch_array($result1)){
                                            $link = "news/news/demo.php?id=".$row1['id'];?>
                                        <li class="in-panel"><a class="five" href="<?php echo $link?>" target="_blank"><?php echo $row1['title']?></a></li><?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4" style="height:570px;">
                            <div class="panel panel-default" style="height:50%;overflow:hidden;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">国际</h3>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    <?php 
                                            $query1 = "SELECT * FROM `wynews`  where category='gj' order by post_data desc LIMIT 5";

                                            $result1 = mysqli_query($dbc,$query1)
                                                or die ("1quering error");
                                            while ($row1=mysqli_fetch_array($result1)){
                                            $link = "news/news/demo.php?id=".$row1['id'];?>
                                        <li class="in-panel"><a class="five" href="<?php echo $link?>" target="_blank"><?php echo $row1['title']?></a></li><?php }?>
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="panel panel-default" style="height:46%;overflow:hidden;">
                                <div class="panel-heading">
                                    <h3 class="panel-title">社会</h3>
                                </div>
                                <div class="panel-body">
                                    <ul>
                                    <?php 
                                            $query1 = "SELECT * FROM `wynews`  where category='sh' order by post_data desc LIMIT 5";

                                            $result1 = mysqli_query($dbc,$query1)
                                                or die ("1quering error");
                                            while ($row1=mysqli_fetch_array($result1)){
                                            $link = "news/news/demo.php?id=".$row1['id'];?>
                                        <li class="in-panel"><a class="five" href="<?php echo $link?>" target="_blank"><?php echo $row1['title']?></a></li><?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>



            </aside>
        </div>
    </div>
    <script type="text/javascript">

    </script>
</body>

</html>