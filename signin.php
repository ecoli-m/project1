<?php     
    //获取表单
    session_start();
    function str_check($str){
        if(!preg_match("/^[A-Za-z0-9]+$/",$str)){
            return TRUE;
        }
        else 
            return FALSE;
    }
    if (isset($_POST['submit'])){
        require_once('conn.php');
        $username = $_POST['username'];
        $password = trim($_POST['password']);
        $password_confirm = trim($_POST['confirm']);
        $checknum = $_POST['checkNum'];
        $stuNum = $_POST['stuNum'];
        $code = $_SESSION['validcode'];
        $feedback = "注册失败";
        $falg = 0;
        if(empty($username)||empty($password)||empty($password_confirm)||empty($checknum)||empty($stuNum)){
            $feedback="请完整填写表单";
        }
        elseif(!preg_match("/^[0-9]*$/",$stuNum)){
            $feedback="学号为纯数字";
        }
        elseif(str_check($username)){
            $feedback="用户名请使用请使用英文或数字";
        }
        elseif($password!=$password_confirm){
            $feedback="两次输入的密码不相同";
        }
        elseif(!preg_match("/^(?![^a-zA-Z]+$)(?!\D+$).{6,16}$/",$password)){
            $feedback="您的密码必须含有英文和数字，不能有特殊字符，长度6到16个字符";
        }
        elseif($checknum!=$code){
            $feedback="验证码错误";
        }
        else{
            $flag = 1;
            $dbc = mysqli_connect(HOST,USER,PASS,DBN)
                or die ("connected error");
            //验证用户名是否重复
            $query = "SELECT * FROM `signin` WHERE username='$username'";
            $result = mysqli_query($dbc,$query)
                    or die ("quering error");
            $row = mysqli_num_rows($result);
            if ($row!=0){
                $feedback="用户名重复";
                $flag = 0;
            }
            //验证学号是否重复
            $query="SELECT * FROM `signin` WHERE stuNum='$stuNum'";
            $result = mysqli_query($dbc,$query)
                    or die ("quering error");
            $row = mysqli_num_rows($result);
            if ($row!=0){
                $feedback="学号重复";
                $flag = 0;
            }
            mysqli_close($dbc);
            if($flag==1){//注册成功转到login页面
                $password = sha1($password);
                $dbc = mysqli_connect(HOST,USER,PASS,DBN)
                    or die ("connected error");
                $query = "INSERT INTO `signin`(`stuNum`,`secretword`, `username`) VALUES ('$stuNum','$password','$username')";
                $result = mysqli_query($dbc,$query)
                    or die ("quering error");
                mysqli_close($dbc);
                header("Location: login.php");
            }
        }
    }
    else {
        $feedback="";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/signin.css" type="text/css">
    <link rel="Shortcut Icon" href="img/favicon.ico" type="image/x-icon" />
    <title>注册成为用户</title>
</head>

<body>
    <div class="layout">
    <main>
        <div id="reg" data-v-ff518656="">
            <!--背景-->
            <div class="reg-bg" data-v-ff518656="">

            </div> 
            <div class="reg-container" data-v-ff518656="">
                <div class="reg-wrapper" data-v-ff518656="">
                    <!--表格页眉-->
                    <div class="reg-head" data-v-ff518656="">
                        <!--***这里是logo图片位置***-->
                        <img src="img/hitsz.png" class="reg-logo" data-v-ff518656="">
                        <!--表格名称--> 
                        <span class="reg-title" data-v-ff518656="">注册</span> 
                        <!--***返回其他页面***-->
                        <a href="login.php" class="return-to-login" data-v-ff518656="">返回登录</a>
                    </div>
                    <!--填表清单-->
                    <div class="reg-form-wrapper" data-v-ff518656="">
                        <div id="signincheck">
                            <?php echo '<p>'.$feedback.'</p>' ;?>
                        </div>
                        <form class="reg-form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                            <!--单个输入元素--> 
                            <div class=" form-item">
                                <!--输入元素样式-->
                                <span class="reg-input input-wrapper input-group input-group-prepend">
                                    <!--***图标***-->
                                    <div class="input-prepend">
                                        <!--图片宽31高41-->
                                        <img src="img/username.PNG">
                                    </div> 
                                    <!--***输入框调整提示语和输入类型***-->
                                    <input placeholder="请输入用户名" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="username" name="username"> 
                                </span> 
                            </div> 
                            <!--单个输入元素--> 
                            <div class=" form-item">
                                <!--输入元素样式-->
                                <span class="reg-input input-wrapper input-group input-group-prepend">
                                    <!--***图标***-->
                                    <div class="input-prepend">
                                        <!--图片宽31高41-->
                                        <img src="img/stuNum.PNG">
                                    </div> 
                                    <!--***输入框调整提示语和输入类型***-->
                                    <input placeholder="请输入学号" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="stuNum" name="stuNum"> 
                                </span> 
                            </div> 
                            <!--单个输入元素--> 
                            <div class=" form-item">
                                <!--输入元素样式-->
                                <span class="reg-input input-wrapper input-group input-group-prepend">
                                    <!--***图标***-->
                                    <div class="input-prepend">
                                        <!--图片宽31高41-->
                                        <img src="img/password.PNG">
                                    </div> 
                                    <!--***输入框调整提示语和输入类型***-->
                                    <input placeholder="请输入密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="password" name="password"> 
                                </span> 
                            </div> 
                            <!--单个输入元素--> 
                            <div class=" form-item">
                                <!--输入元素样式-->
                                <span class="reg-input input-wrapper input-group input-group-prepend">
                                    <!--***图标***-->
                                    <div class="input-prepend">
                                        <!--图片宽31高41-->
                                        <img src="img/password.PNG">
                                    </div> 
                                    <!--***输入框调整提示语和输入类型***-->
                                    <input placeholder="请再次输入密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="confirm" name="confirm"> 
                                </span> 
                            </div> 
                            <!--验证码元素-->
                            <div class="form-item">
                                <span class="reg-input reg-textcode input-wrapper input-group input-group-prepend input-suffix">
                                    <div class="input-prepend">
                                        <div class="input-prepend">
                                            <!--图片宽31高41-->
                                            <img src="img/checkNum.PNG">
                                        </div> 
                                    </div> 
                                    <!--***验证码输入框***-->
                                    <input placeholder="请输入验证码" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="checkNum" name="checkNum"> 
                                    <div class="input-suffix-item">       
                                        <a href="javascript:changeCode()" class=" get-text-code">
                                            <img src="validcode.php" style="width:100px;height:25px;" id="code">
                                            看不清，换一张
                                        </a>
                                    </div>
                                </span> 
                            </div>
                            <!--***提交按钮***-->
                                <button type="submit" value="注册" name="submit" class="reg-submit">
                                    注册
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
        <script src="http://libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
        <script type="text/javascript">
            //点击图片更新验证码
            function changeCode() {

               document.getElementById("code").src = "validcode.php?id=" + Math.random();
            }
        </script>
    </body>
</html>