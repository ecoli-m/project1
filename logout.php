<?php 
	header('Content-type:text/html; charset=utf-8');
	// 注销后的操作
	session_start();
	// 清除Session
	$stuNum = $_SESSION['stuNum'];  //用于后面的提示信息
	$_SESSION = array();
	session_destroy();
 
	// 清除Cookie
	setcookie('stuNum', '', time()-99);
	setcookie('code', '', time()-99);
 
	// 提示信息
	header("location:login.php");
 
 ?>

