<?php
    session_start();
    if (isset($_POST['submit'])){
        //对前台传来的数据进行特殊字符的转义，能够有效的防止sql注入等
            $stuNum =trim($_POST['stuNum']);
            $password = sha1(trim($_POST['password']));
            $checkNum = $_POST['checkNum'];
            require_once("loginsql.php");
            $feedback = "学号或密码错误";
            if (empty($_POST['stuNum'])){
                $feedback= "请输入学号";
                }
            elseif(empty($_POST['password'])){
                $feedback="请输入密码";
            }
            elseif (!isset($_POST['checkNum'])){
                $feedback="请输入验证码";
            }
            elseif($checkNum!=$_SESSION["validcode"]){
                $feedback="验证码错误";
            }
            else{
                if ($row==1){
                    $_SESSION['stuNum'] = $stuNum;
                    $_SESSION['islogin'] = 1;
                    if ($_POST['remember'] == "yes") {
                        setcookie('stuNum', $stuNum, time()+7*24*60*60);
                        setcookie('code', sha1($stuNum.sha1($password)), time()+7*24*60*60);
                    } 
                    else {
                        // 没有勾选则保持Cookie存在2个小时
                        setcookie('stuNum', $stuNum, time()+2*60*60);
                        setcookie('code', sha1($stuNum.sha1($password)), time()+2*60*60);
                    }
                    header("location: admin.php");
                }
            }
    }
    else{
        $feedback="";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/login.css" type="text/css">
        <link rel="Shortcut Icon" href="img/favicon.ico" type="image/x-icon" />
        <title>登录</title>
    </head>
    <body> 
        <div class="layout">
            <main>
                <div id="reg" data-v-ff518656="">
                    <!--背景-->
                    <div class="reg-bg" data-v-ff518656="">
        
                    </div> 
                    <div class="reg-container" data-v-ff518656="">
                        <div class="reg-wrapper" data-v-ff518656="">
                            <!--表格页眉-->
                            <div class="reg-head" data-v-ff518656="">
                                <!--***这里是logo图片位置***-->
                                <img src="img/hitsz.jpeg" class="reg-logo" data-v-ff518656="">
                                <!--***返回其他页面***-->
                                <a href="signin.php" class="login" data-v-ff518656="">新用户注册</a>
                            </div>
                            <!--填表清单-->
                            <div class="reg-form-wrapper" data-v-ff518656=""> 
                                <div id="logincheck">
                                    <?php echo '<p>'.$feedback.'</p>' ;?>
                                </div>
                                <form class="reg-form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
                                    
                                    <!--单个输入元素--> 
                                    <div class=" form-item1">
                                        <!--输入元素样式-->
                                        <span class="reg-input input-wrapper input-group input-group-prepend">
                                            <!--***图标***-->
                                            <div class="input-prepend">
                                                <!--图片宽31高41-->
                                                <img src="img/username.PNG">
                                            </div> 
                                            <!--***输入框调整提示语和输入类型***-->
                                            <input placeholder="请输入学号" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="stuNum" name="stuNum"> 
                                        </span> 
                                    </div> 
                                    <!--单个输入元素--> 
                                    <div class=" form-item">
                                        <!--输入元素样式-->
                                        <span class="reg-input input-wrapper input-group input-group-prepend">
                                            <!--***图标***-->
                                            <div class="input-prepend">
                                                <!--图片宽31高41-->
                                                <img src="img/password.PNG">
                                            </div> 
                                            <!--***输入框调整提示语和输入类型***-->
                                            <input placeholder="请输入密码" autocomplete="new-password" minlength="" maxlength="" type="password" value="" class="input" id="password" name="password"> 
                                        </span> 
                                    </div> 
                                    <!--验证码元素-->
                                    <div class="form-item">
                                        <span class="reg-input reg-textcode input-wrapper input-group input-group-prepend input-suffix">
                                            <div class="input-prepend">
                                                <div class="input-prepend">
                                                    <!--图片宽31高41-->
                                                    <img src="img/checkNum.PNG">
                                                </div> 
                                            </div> 
                                            <!--***验证码输入框***-->
                                            <input placeholder="请输入验证码" autocomplete="new-password" minlength="" maxlength="" type="text" value="" class="input" id="checkNum" name="checkNum"> 
                                            <div class="input-suffix-item">       
                                                <a href="javascript:changeCode()" class=" get-text-code">
                                                <img src="validcode.php" style="width:100px;height:25px;" id="code"/>
                                                    看不清，换一张
                                                </a>
                                            </div>
                                        </span> 
                                    </div>
                                    <!--***提交按钮***-->
                                    <button type="submit" value="登录" name="submit" class="reg-submit">
                                        登录
                                    </button>
                                    <!--勾选-->
                                    <div class="reg-agree">
                                        <label class="reg-g-checkbox-wrapper">
                                            <span class="reg-g-checkbox">
                                                <input id="remember" type="checkbox" value="yes" class="reg-g-checkbox-input" name="remember"> 
                                                <span class="reg-g-checkbox-inner">

                                                </span>
                                            </span> 
                                            <span class="reg-g-checkbox-text reg-check-text">
                                                7天内自动登录
                                            </span> 
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        </div>
                <script src="http://libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript">
                    //点击图片更新验证码
                    function changeCode() {
        
                       document.getElementById("code").src = "validcode.php?id=" + Math.random();
                    }
                </script>
            </body>
        </html>
